import javax.swing.JOptionPane;

public class MenuPrincipal {
	
	Computadora computadora;
	
	public MenuPrincipal(){
		
		menu();
		
		}//fin metodo constructor SP
		
	public void menu(){
		
		char opcion;
		int desicion=0;
		do{
			
			opcion=(JOptionPane.showInputDialog("*********************** Menu principal *******************************\n"+
			
			"a. Crear instancia de computadora sin valores\n"+
			"b. Crear instancia de computadora con los valores necesarios para cada uno de sus atributos\n"+
			"c. Modificar la placa de la computadora\n"+
			"d. Modificar el modelo de la computadora\n"+
			"e. Modificar el tamano de la pantalla de la computadora\n"+
			"f. Modificar el estado de la computadora\n"+
			"g. Ver los datos de la computadora.\n"+
			"h. Salir")).charAt(0);
			
			switch (opcion){
				
				case 'a':
				{
					if(desicion==0){
					computadora=new Computadora();
					desicion=1;
				}//fin if
				else {
					
					JOptionPane.showMessageDialog(null,"La instancia ya ha sido creada, modifiquela con otra opcion del menu");
					
					}//fin else
					//computadora.Computadora();
					
					}break;
					
				case 'b':
				{
					if (desicion==0){
					
					String modelo, placa;
					int pantalla;
					
					modelo=JOptionPane.showInputDialog("Digite el modelo de la computadora");
					placa=JOptionPane.showInputDialog("Digite la placa de la computadora");
					pantalla=Integer.parseInt(JOptionPane.showInputDialog("Digite el tamaño de la pantalla de la computadora"));
					
					computadora=new Computadora(modelo, placa, pantalla);
					
					desicion=1;
				}//fin if
				else{
					
					JOptionPane.showMessageDialog(null,"La instancia ya ha sido creada, modifiquela con otra opcion del menu");
					
					}//
		
					//computadora.Computadora(modelo, placa, pantalla); 
					
					}break;
					
				case 'c':
				{
					if(desicion==1){

					computadora.setPlaca(JOptionPane.showInputDialog("Digite la placa de la computadora"));
				}//fin if
				
				else{
					
					JOptionPane.showMessageDialog(null,"La instancia no ha sido creada, crearla con otra opcion del menu");
					
					}//fin else
					
					}break;
					
				case 'd':
				{
					if(desicion==1){

					computadora.setModelo(JOptionPane.showInputDialog("Digite el modelo de la computadora"));
				}//fin if
				
				else{
					
					JOptionPane.showMessageDialog(null,"La instancia no ha sido creada, crearla con otra opcion del menu");
					
					}//fin else
				
					
					}break;
					
				case 'e':
				{
					if(desicion==1){

					computadora.setPantalla(Integer.parseInt(JOptionPane.showInputDialog("Digite el tamaño de la pantalla de la computadora")));
				}//fin if
				
				else{
					
					JOptionPane.showMessageDialog(null,"La instancia no ha sido creada, crearla con otra opcion del menu");
					
					}//fin else
								
					
					}break;
					
				case 'f':
				{
					if(desicion==1){

					computadora.setEstado(JOptionPane.showInputDialog("Digite el estado de la computadora"));
				}//fin if
				
				else{
					
					JOptionPane.showMessageDialog(null,"La instancia no ha sido creada, crearla con otra opcion del menu");
					
					}//fin else
					
					}break;
					
				case 'g':
				{
					if(desicion==1){

					JOptionPane.showMessageDialog(null,computadora.toString());
				}//fin if
				
				else{
					
					JOptionPane.showMessageDialog(null,"La instancia no ha sido creada, crearla con otra opcion del menu");
					
					}//fin else
					
					}break;
					
				case 'h':
				{
					JOptionPane.showMessageDialog(null,"Gracias por usar esta aplicacion");
					
					}break;
				
				default: {
					
					JOptionPane.showMessageDialog(null,"Digite una opcion valida del menu");
					
					}break;
				}//fin switch
 
			
			}while (opcion!='h');
		}

public static void main (String arg[]){
	
	MenuPrincipal menuPrincipal=new MenuPrincipal();
	
	}//fin metodo main

}//fin clase
